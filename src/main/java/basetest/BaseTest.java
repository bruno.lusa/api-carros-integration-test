package basetest;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import com.lusa.carros.clients.DeleteCarrosClient;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

@Listeners({ExtentITestListenerClassAdapter.class})
public class BaseTest {

    private static RequestSpecification requestSpecification;

    protected DeleteCarrosClient deleteCarrosClient;

    @BeforeClass
    public void setUp() {

        deleteCarrosClient = new DeleteCarrosClient();

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
