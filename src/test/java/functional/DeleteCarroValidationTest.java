package functional;

import basetest.BaseTest;
import builder.Carro;
import datadriven.CarroDataProvider;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_CREATED;
import static requestspecification.CarrosRequestSpecification.getRequestSpecification;

public class DeleteCarroValidationTest extends BaseTest {

    @Test(dataProvider = "novoCarro", dataProviderClass = CarroDataProvider.class)
    public void validaDelecaodoCarro(Carro carroProvider){
        Carro carroInserido =
        given().
            spec(getRequestSpecification()).
            body(carroProvider).
        when().
            post("/carros").
        then().
            statusCode(SC_CREATED).
            extract().
                body().
                    as(Carro.class);

        ValidatableResponse response = deleteCarrosClient.deleteCarros(carroInserido.getId());

    }
}
